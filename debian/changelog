libpostscript-perl (0.06-4) UNRELEASED; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 16:44:14 +0100

libpostscript-perl (0.06-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 00:10:39 +0100

libpostscript-perl (0.06-3) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Remove David Paleino from Uploaders on his request.
  * debian/rules: switch order of arguments to dh.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Mark package as source format 3.0 (quilt)
  * Add to and forward fix-pod.patch
  * Update license paragraphs to commonly used versions
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.5
  * Mark package autopkgtest-able
  * Add cant-modify-constant-item.patch

 -- Florian Schlichting <fsfs@debian.org>  Sun, 08 Jul 2018 11:57:18 +0200

libpostscript-perl (0.06-2) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Recommend ghostcript instead of obsolete gs-gpl (closes: #539668).
  * Set Standards-Version to 3.8.2; add debian/README.source to document
    quilt usage.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * Switch to debhelper 7, adjust debian/{rules,compat,control}.
  * Add /me to Uploaders.
  * Add headers to fix-interpreter.patch.
  * Minor changes to short and long description.
  * debian/copyright: switch to new format.
  * New patch fix-pod.patch: fix errors from pod2man.

 -- gregor herrmann <gregoa@debian.org>  Sun, 09 Aug 2009 19:00:04 +0200

libpostscript-perl (0.06-1) unstable; urgency=low

  [ David Paleino ]
  * Initial Release (Closes: #460622)

 -- Roberto C. Sanchez <roberto@debian.org>  Mon, 21 Jan 2008 14:25:54 -0500
